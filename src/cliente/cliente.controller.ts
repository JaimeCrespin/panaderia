import {Controller, Get, Post, Res, Body, Param, Req} from '@nestjs/common';
import {ClientesService} from './cliente.service';
import {Cliente} from "./interfaces/cliente";

@Controller('/panaderia')
export class  ClienteController {
  constructor(private readonly _clientesService: ClientesService ) {

  }
  @Get('crear')
  formulariocrearCliente(
    @Res() res,
  ) {
    res.render('crear_cliente');
  }
  @Post('crear')
  crearCliente(
    @Body() clienteACrear: Cliente,
    @Res() res) {

    clienteACrear.edad = Number(clienteACrear.edad);
    this._clientesService.crear(clienteACrear);

    res.redirect('/panaderia/lista');
  }
  @Get('lista')
  listarCliente(@Res() res) {
    const arregloCliente = this._clientesService.bddClientes;
    res.render('listar_cliente', {arregloCliente: arregloCliente});
  }
  @Get('editar/:id')
  formularioEditarCliente(
    @Param('id') idCliente: string,
    @Res() res) {

    var clienteaEditar = this._clientesService.buscarPorId(+idCliente);
    res.render('editar_cliente', {clienteaEditar: clienteaEditar});
  }
  @Post('editar/:id')
  editarCliente(
    @Param('id') idCliente: string,
    @Body() clienteaEditar: Cliente,
    @Res() res) {

    this._clientesService.actualizar(clienteaEditar, +idCliente);
    res.redirect('/panaderia/lista');
  }

}
