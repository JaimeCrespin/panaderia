import {Module} from '@nestjs/common';
import {ClienteController} from './cliente.controller';
import {ClientesService} from './cliente.service';

@Module({
  imports: [],  // Modulos
  controllers: [
    ClienteController,
  ], // Controladores
  providers: [
    ClientesService,
  ], // Servicios
  exports: [
    ClientesService,
  ],
})
export class ClienteModule {
}
