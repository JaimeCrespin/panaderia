import {Injectable} from '@nestjs/common';
import {Cliente} from './interfaces/cliente';

@Injectable()
export class ClientesService {

  bddClientes = [];
  recnum = 1;

  constructor() {
    const cliente = {
      nombre: 'Unkwnown',
      edad: 10,
      cedula: 1317051587,

    };
    this.listar(cliente);
  }

  listar(nuevoCliente) {
    nuevoCliente.id = this.recnum;
    this.recnum++;
    this.bddClientes.push(nuevoCliente);
    return nuevoCliente;
  }

  crear(nuevoCliente: Cliente): Cliente {
    nuevoCliente.id = this.recnum;
    this.recnum++;
    this.bddClientes.push(nuevoCliente);
    return nuevoCliente;
  }

  buscarPorId(id: number): Cliente {
    return this.bddClientes.find(
      (cliente) => {
        return cliente.id === id;
      },
    );
  }

  buscarPorNombre(nombre: string): Cliente[] {
    if (nombre !== '' && nombre !== null) {
      return this.bddClientes.filter(
        (cliente) => {
          return cliente.nombre.toUpperCase().includes(nombre.toUpperCase());
        },
      );
      console.log(nombre);
    } else {
      return this.bddClientes;
    }
  }

  eliminarPorId(id: number): Cliente[] {
    console.log('id', id);
    const indice = this.bddClientes.findIndex(
      (libro) => {
        return libro.id === id;
      },
    );
    this.bddClientes.splice(indice, 1);
    return this.bddClientes;
  }

  actualizar(libroActualizado: Cliente, id: number): Cliente[] {

    const indice = this.bddClientes.findIndex(
      (libro) => {
        return libro.id === id;
      },
    );
    console.log(indice);
    libroActualizado.id = this.bddClientes[indice].id;
    this.bddClientes[indice] = libroActualizado;
    return this.bddClientes;
  }

}
