export interface Cliente {
  id?: number;
  nombre: string;
  edad: number;
  cedula: string;
}
