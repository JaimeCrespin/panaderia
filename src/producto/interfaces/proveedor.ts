export interface Proveedor {
  id?: number;
  nombre: string;
  RUC: string;
  direccion: string;
}
