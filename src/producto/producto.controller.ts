import {Controller, Get, Post, Res, Body, Param, Req} from '@nestjs/common';
import {ProveedorService} from './proveedor.service';
import {Proveedor} from "./interfaces/proveedor";

@Controller('/panaderia')
export class  ProveedorController {
  constructor(private readonly _proveedorService: ProveedorService ) {

  }
  @Get('crearProveedor')
  formulariocrearProveedor(
    @Res() res,
  ) {
    res.render('crear_proveedor');
  }

  @Post('crearProveedor')
  crearProveedor(
    @Body() proveedoraCrear: Proveedor,
    @Res() res) {
    this._proveedorService.crear(proveedoraCrear);

    res.redirect('/panaderia/listaProveedor');
  }
  @Get('listaProveedor')
  listarProveedor(@Res() res) {
    const arregloProveedor = this._proveedorService.bddProveedor;
    res.render('listar_proveedor', {arregloProveedor: arregloProveedor});
  }


  @Get('editarProveedor/:id')
  formularioEditarProveedor(
    @Param('id') idProveedor: string,
    @Res() res) {

    var proveedoraEditar = this._proveedorService.buscarPorId(+idProveedor);
    res.render('editar_proveedor', {proveedoraEditar: proveedoraEditar});
  }
  @Post('editarProveedor/:id')
  editarProveedor(
    @Param('id') idProveedor: string,
    @Body() proveedoraEditar: Proveedor,
    @Res() res) {

    this._proveedorService.actualizar(proveedoraEditar, +idProveedor);
    res.redirect('/panaderia/listaProveedor');
  }
  @Post('eliminarProveedor/:id')
  eliminarProveedor(@Res() res,
                @Body('id') id: number) {
    this._proveedorService.eliminarPorId(id);
    res.redirect('/panaderia/listaProveedor');
  }

  @Post('buscar_proveedor')
  buscarCliente(
      @Body ('busqueda') nombreProveedor: string,
      @Res() res) {

    const listaProveedor: Proveedor[] = this._proveedorService.buscarPorNombre(nombreProveedor);

    if(listaProveedor != null) {
      res.render('listar_proveedor', {arregloProveedor: listaProveedor});
    } else {
      res.redirect('/panaderia/listaProveedor');
    }
  }

}
