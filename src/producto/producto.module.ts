import {Module} from '@nestjs/common';
import {ProveedorController} from './proveedor.controller';
import {ProveedorService} from './proveedor.service';

@Module({
  imports: [],  // Modulos
  controllers: [
    ProveedorController,
  ], // Controladores
  providers: [
    ProveedorService,
  ], // Servicios
  exports: [
    ProveedorService,
  ],
})
export class ProveedorModule {
}
