import {Injectable} from '@nestjs/common';
import {Proveedor} from './interfaces/proveedor';

@Injectable()
export class ProveedorService {

  bddProveedor = [];
  recnum = 1;

  constructor() {
    const proveedor = {
      nombre: 'Proveedor1',
      RUC: '1317051587',
      direccion: 'N/A',

    };
    this.listar(proveedor);
  }

  listar(nuevoProveedor) {
    nuevoProveedor.id = this.recnum;
    this.recnum++;
    this.bddProveedor.push(nuevoProveedor);
    return nuevoProveedor;
  }

  crear(nuevoProveedor: Proveedor): Proveedor {
    nuevoProveedor.id = this.recnum;
    this.recnum++;
    this.bddProveedor.push(nuevoProveedor);
    return nuevoProveedor;
  }

  buscarPorId(id: number): Proveedor {
    return this.bddProveedor.find(
      (proveedor) => {
        return proveedor.id === id;
      },
    );
  }

  buscarPorNombre(nombre: string): Proveedor[] {
    if (nombre !== '' && nombre !== null) {
      return this.bddProveedor.filter(
        (proveedor) => {
          return proveedor.nombre.toUpperCase().includes(nombre.toUpperCase());
        },
      );
    } else {
      return this.bddProveedor;
    }
  }

  eliminarPorId(id: number): Proveedor[] {
    const indice = this.bddProveedor.findIndex(
      (proveedor) => {
        return proveedor.id === id;
      },
    );
    this.bddProveedor.splice(indice, 1);
    return this.bddProveedor;
  }

  actualizar(proveedorActualizado: Proveedor, id: number): Proveedor[] {

    const indice = this.bddProveedor.findIndex(
      (cliente) => {
        return cliente.id === id;
      },
    );
    proveedorActualizado.id = this.bddProveedor[indice].id;
    this.bddProveedor[indice] = proveedorActualizado;
    return this.bddProveedor;
  }

}
