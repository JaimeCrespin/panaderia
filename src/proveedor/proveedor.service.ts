import {Injectable} from '@nestjs/common';
import {Cliente} from './interfaces/cliente';

@Injectable()
export class ClientesService {

  bddClientes = [];
  recnum = 1;

  constructor() {
    const cliente = {
      nombre: 'Unkwnown',
      edad: 10,
      cedula: 1317051587,

    };
    this.listar(cliente);
  }

  listar(nuevoCliente) {
    nuevoCliente.id = this.recnum;
    this.recnum++;
    this.bddClientes.push(nuevoCliente);
    return nuevoCliente;
  }

  crear(nuevoCliente: Cliente): Cliente {
    nuevoCliente.id = this.recnum;
    this.recnum++;
    this.bddClientes.push(nuevoCliente);
    return nuevoCliente;
  }

  buscarPorId(id: number): Cliente {
    return this.bddClientes.find(
      (cliente) => {
        return cliente.id === id;
      },
    );
  }

  buscarPorNombre(nombre: string): Cliente[] {
    if (nombre !== '' && nombre !== null) {
      return this.bddClientes.filter(
        (cliente) => {
          return cliente.nombre.toUpperCase().includes(nombre.toUpperCase());
        },
      );
    } else {
      return this.bddClientes;
    }
  }

  eliminarPorId(id: number): Cliente[] {
    const indice = this.bddClientes.findIndex(
      (cliente) => {
        return cliente.id === id;
      },
    );
    this.bddClientes.splice(indice, 1);
    return this.bddClientes;
  }

  actualizar(clienteActualizado: Cliente, id: number): Cliente[] {

    const indice = this.bddClientes.findIndex(
      (cliente) => {
        return cliente.id === id;
      },
    );
    clienteActualizado.id = this.bddClientes[indice].id;
    this.bddClientes[indice] = clienteActualizado;
    return this.bddClientes;
  }

}
